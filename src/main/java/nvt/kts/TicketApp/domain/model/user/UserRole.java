package nvt.kts.TicketApp.domain.model.user;

public enum UserRole {
    ADMIN, REGISTERED
}
