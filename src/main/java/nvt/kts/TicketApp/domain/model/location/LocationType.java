package nvt.kts.TicketApp.domain.model.location;

public enum LocationType {
    STADIUM, HALL, ARENA, OPEN
}
